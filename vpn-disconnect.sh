#!/bin/bash
# place this script inside  /etc/NetworkManager/dispatcher.d directory
# echo below sends args for debug to a file inside /tmp so if you need it uncomment it
echo "1 $1 2 $2" > /tmp/vpn-down-args-$(date +%F_%T).txt
interface=$1
state=$2
if [[ $state == "vpn-down" ]]; then
    touch /tmp/vpn-down
    sudo service nscd restart
fi

if [[ ($interface == "tun0" || $interface == "ppp0") && ($state == "up" || $state == "vpn-up") ]]; then
    touch /tmp/vpn-up
    sudo service nscd restart
fi
