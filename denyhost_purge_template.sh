#!/bin/bash
######################################################################
# place this script's path in the PLUGIN_PURGE variable in denyhosts.conf#
# PLUGIN_PURGE: If set, this value should point to an executable
# program that will be invoked when a host is removed from the
# HOSTS_DENY file.  This executable will be passed the host
# that is to be purged as it's only argument.
#
PLUGIN_PURGE=/usr/sbin/denyhost_purge.sh
#
######################################################################
HOST=$1
firewall-cmd --remove-rich-rule='rule family=ipv4 source address='$HOST' drop'
touch /tmp/run_denyhost_purge_$HOST
