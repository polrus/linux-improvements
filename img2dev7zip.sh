#!/bin/bash

# Check if the correct number of arguments is provided
if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <source_file.7z> <output_device_or_file>"
  echo
  echo "Arguments:"
  echo "  <source_file.7z>          The path to the source .7z archive file."
  echo "  <output_device_or_file>   The output device (e.g., /dev/sda) or file where data will be written."
  echo
  echo "Example:"
  echo "  $0 rpi44-backup.tar.7z /dev/sda"
  exit 1
fi

# Assign arguments to variables for easier reference
SOURCE_FILE=$1
OUTPUT_DEVICE=$2

# Function to suggest installation commands
suggest_install() {
  echo "You can install the missing tool using one of the following commands:"
  echo "  For Debian/Ubuntu-based systems: sudo apt-get install $1"
  echo "  For openSUSE systems: sudo zypper install $1"
}

# Check if 7z is installed
if ! command -v 7z &> /dev/null; then
  echo "Error: '7z' command is not found."
  suggest_install "p7zip"
  exit 1
fi

# Check if pv is installed
if ! command -v pv &> /dev/null; then
  echo "Error: 'pv' command is not found."
  suggest_install "pv"
  exit 1
fi

# Check if the source file exists
if [ ! -f "$SOURCE_FILE" ]; then
  echo "Error: Source file '$SOURCE_FILE' does not exist."
  exit 1
fi

# Use pv to monitor the progress and write to the output device/file
echo "Extracting '$SOURCE_FILE' to '$OUTPUT_DEVICE'..."
7z x -so "$SOURCE_FILE" | pv -s $(stat -c%s "$SOURCE_FILE") | sudo dd of="$OUTPUT_DEVICE" bs=4M status=progress

echo "Operation completed successfully."
