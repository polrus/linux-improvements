#!/bin/bash

# Check if the correct number of arguments is provided
if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <source_device> <destination_file.7z>"
  echo
  echo "Arguments:"
  echo "  <source_device>           The source device (e.g., /dev/sda)."
  echo "  <destination_file.7z>     The destination compressed image file (.7z)."
  echo
  echo "Example:"
  echo "  $0 /dev/sda rpi44-backup.img.7z"
  exit 1
fi

# Function to suggest installation commands
suggest_install() {
  echo "You can install the missing tool using one of the following commands:"
  echo "  For Debian/Ubuntu-based systems: sudo apt-get install $1"
  echo "  For openSUSE systems: sudo zypper install $1"
}

# Check if 7z is installed
if ! command -v 7z &> /dev/null; then
  echo "Error: '7z' command is not found."
  suggest_install "p7zip"
  exit 1
fi

# Check if pv is installed
if ! command -v pv &> /dev/null; then
  echo "Error: 'pv' command is not found."
  suggest_install "pv"
  exit 1
fi

# Assign arguments to variables for easier reference
SOURCE_DEVICE=$1
DESTINATION_FILE=$2

# Check if the source device exists
if [ ! -b "$SOURCE_DEVICE" ]; then
  echo "Error: Source device '$SOURCE_DEVICE' does not exist."
  exit 1
fi

# Check if the destination file has the .7z extension, if not add it
if [[ "$DESTINATION_FILE" != *.7z ]]; then
  DESTINATION_FILE="${DESTINATION_FILE}.7z"
fi

# Get the size of the source device
DEVICE_SIZE=$(blockdev --getsize64 "$SOURCE_DEVICE")
# DEVICE_SIZE=32212254720
# Check if device size could be determined
if [ -z "$DEVICE_SIZE" ]; then
  echo "Error: Unable to determine the size of the device '$SOURCE_DEVICE'."
  exit 1
fi

# Use pv to monitor progress while copying from device to an image file and compressing with 7zip
echo "Creating an image from device '$SOURCE_DEVICE' and compressing to '$DESTINATION_FILE'..."
sudo dd if="$SOURCE_DEVICE" bs=4M | pv -s "$DEVICE_SIZE" | 7z a -t7z -si "$DESTINATION_FILE"

echo "Operation completed successfully."
