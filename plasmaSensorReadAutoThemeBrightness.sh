#!/bin/bash

# theme_switcher v 3.5 with Hysteresis, Interval Loop, Auto Screen Brightness, and KDE Brightness Control

# File to store the last theme to prevent repeated commands
theme_state_file="$HOME/.current_theme_state"

# File to store the last brightness level to prevent unnecessary adjustments
brightness_state_file="$HOME/.current_brightness_state"

# Hysteresis parameter
hysteresis=0.05  # Adjust this value to control how much the brightness needs to change to switch themes

# Brightness threshold
threshold=0.25  # Default threshold

# Interval between checks (in seconds)
interval=5  # Set interval to 5 seconds

# Function to set KDE theme
set_theme() {
    local theme=$1
    if [[ "$theme" == "light" ]]; then
        lookandfeeltool -a org.kde.breeze.desktop
        echo "light" > "$theme_state_file"
    elif [[ "$theme" == "dark" ]]; then
        lookandfeeltool -a org.kde.breezedark.desktop
        echo "dark" > "$theme_state_file"
    fi
}

# Function to read the last theme state
get_last_theme() {
    if [[ -f "$theme_state_file" ]]; then
        cat "$theme_state_file"
    else
        echo "none"  # Default if no previous state is stored
    fi
}

# Function to read the last brightness level
get_last_brightness() {
    if [[ -f "$brightness_state_file" ]]; then
        cat "$brightness_state_file"
    else
        echo "none"  # Default if no previous state is stored
    fi
}

# Function to set screen brightness
auto_set_brightness() {
    local brightness=$1
    local last_brightness=$(get_last_brightness)

    # Adjust brightness value to be between 0.1 and 1.0 (assuming screen brightness control range)
    adjusted_brightness=$(awk -v b="$brightness" 'BEGIN {if (b < 0.1) b = 0.1; if (b > 1.0) b = 1.0; print b}')
    brightness_value=$(awk -v b="$adjusted_brightness" 'BEGIN {print int(b * 10000)}')

    # Apply hysteresis for brightness change
    if [[ "$last_brightness" == "none" ]] || (( $(awk -v b1="$brightness_value" -v b2="$last_brightness" -v h="$hysteresis" 'BEGIN {print (b1 > b2 + h*10000 || b1 < b2 - h*10000)}') )); then
        echo "Setting screen brightness to: $adjusted_brightness"
        echo "Setting screen brightness value to: $brightness_value"
        # Set the brightness using KDE command for Wayland
        qdbus6 org.kde.Solid.PowerManagement /org/kde/Solid/PowerManagement/Actions/BrightnessControl org.kde.Solid.PowerManagement.Actions.BrightnessControl.setBrightness "$brightness_value"
        echo "$brightness_value" > "$brightness_state_file"
    else
        echo "Brightness change within hysteresis range, no change applied."
    fi
}

# Main loop to repeatedly check brightness and update theme
while true; do
    # Read brightness from illuminance sensor
    if [[ -f /sys/bus/iio/devices/iio:device0/in_illuminance_raw ]]; then
        brightness=$(cat /sys/bus/iio/devices/iio:device0/in_illuminance_raw)
        # Normalize brightness value to a range between 0 and 1 (assuming maximum value is 3000000)
        brightness=$(awk -v b="$brightness" 'BEGIN {print b / 200000}')

        # Retrieve the last applied theme
        last_theme=$(get_last_theme)

        # Determine the theme based on brightness and hysteresis
        threshold_high=$(awk -v t="$threshold" -v h="$hysteresis" 'BEGIN {print t + h}')
        threshold_low=$(awk -v t="$threshold" -v h="$hysteresis" 'BEGIN {print t - h}')

        echo "Current brightness: $brightness"
        echo "Threshold high: $threshold_high"
        echo "Threshold low: $threshold_low"

        brightness_float=$(awk -v b="$brightness" 'BEGIN {printf "%.6f", b}')

        # Set screen brightness automatically based on the captured brightness
        auto_set_brightness "$brightness_float"

        if (( $(awk -v b="$brightness_float" -v th="$threshold_high" 'BEGIN {print (b > th)}') )); then
            if [[ "$last_theme" != "light" ]]; then
                echo "Detected Bright Environment: Switching to Light Theme"
                set_theme "light"
            else
                echo "Light theme is already active. No change needed."
            fi
        elif (( $(awk -v b="$brightness_float" -v tl="$threshold_low" 'BEGIN {print (b < tl)}') )); then
            if [[ "$last_theme" != "dark" ]]; then
                echo "Detected Dark Environment: Switching to Dark Theme"
                set_theme "dark"
            else
                echo "Dark theme is already active. No change needed."
            fi
        else
            echo "Brightness within hysteresis range, no theme change required."
            increase_needed=$(awk -v b="$brightness_float" -v th="$threshold_high" 'BEGIN {print (th - b)}')
            decrease_needed=$(awk -v b="$brightness_float" -v tl="$threshold_low" 'BEGIN {print (b - tl)}')
            if (( $(awk -v inc="$increase_needed" 'BEGIN {print (inc > 0)}') )); then
                echo "Increase brightness by $increase_needed to switch to light theme."
            fi
            if (( $(awk -v dec="$decrease_needed" 'BEGIN {print (dec > 0)}') )); then
                echo "Decrease brightness by $decrease_needed to switch to dark theme."
            fi
        fi
    else
        echo "Error: Unable to read illuminance value from sensor."
    fi

    # Wait for the defined interval before checking again
    sleep "$interval"
done

exit 0
