#!/bin/bash

######################################################################
# place this script's path in the PLUGIN_DENY variable in denyhosts.conf
# PLUGIN_DENY: If set, this value should point to an executable
# program that will be invoked when a host is added to the
# HOSTS_DENY file.  This executable will be passed the host
# that will be added as it's only argument.
#
#PLUGIN_DENY=/usr/sbin/denyhost_block.sh
#
######################################################################


HOST=$1
firewall-cmd --add-rich-rule='rule family=ipv4 source address='$HOST' drop'
echo "blocking $HOST" > /tmp/run_denyhost_block_$HOST
# if you want you can use smtp email client to send notifycation using github.com:deanproxy/eMail.git #check readmy and config file, you may use eMail config file instead of providing details below

email -s "denyhost new entry" -u$SMTP_USER -i$SMTP_PASS -r$SMTP_HOST -p587 -tls -mlogin -f$SMTP_USER  -n$RECIPIENT < /tmp/run_denyhost_block_$HOST
